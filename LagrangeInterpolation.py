import csv
import os
import math

from matplotlib import pyplot


def interpolation_function(points):
    def function(x):
        result = 0.0
        n = len(points)
        for i in range(n):
            xi, yi = points[i]
            base = 1
            for j in range(n):
                if i != j:
                    xj, yj = points[j]
                    base *= (float(x) - float(xj)) / (float(xi) - float(xj))
            result += float(yi) * base
        return result

    return function


def interpolate_with_lagrange(k):
    for file in os.listdir(os.path.join('.', 'data')):
        with open(os.path.join('.', 'data', file), 'r') as f:
            raw_data = list(csv.reader(f))
            interpolation_data = raw_data[1::k]

            func = interpolation_function(interpolation_data)

            distance = []
            height = []
            interpolated_height = []
            for point in raw_data[1:]:
                x, y = point
                distance.append(float(x))
                height.append(float(y))
                interpolated_height.append(func(float(x)))

            train_distance = []
            train_height = []
            for point in interpolation_data:
                x, y = point
                train_distance.append(float(x))
                train_height.append(func(float(x)))

            n = math.floor(len(distance)/3)
            #
            pyplot.plot(distance[n:2*n], height[n:2*n], 'r.', label='pełne dane')
            pyplot.plot(distance[n:2*n], interpolated_height[n:2*n], color='blue', label='funkcja interpolująca')
            pyplot.plot(train_distance[n:2*n], train_height[n:2*n], 'g.', label='dane do interpolacji')

            # pyplot.semilogy(distance, height, 'r.', label='pełne dane')
            # pyplot.semilogy(distance, interpolated_height, color='blue', label='funkcja interpolująca')
            # pyplot.semilogy(train_distance, train_height, 'g.', label='dane do interpolacji')
            pyplot.legend()
            pyplot.ylabel('Wysokość')
            pyplot.xlabel('Odległość')
            pyplot.title(f'Przybliżenie interpolacją Lagrange\'a, {str(len(interpolation_data))} punktów')
            pyplot.suptitle(file)
            pyplot.grid()

            filename, _ = os.path.splitext(file)
            path = os.path.join('.', 'plots', 'lagrange')
            if not os.path.exists(path):
                os.makedirs(path)
            pyplot.savefig(os.path.join(path, f'{filename}_bez_rungego.png'))

            pyplot.show()
