def vector_zeros(length):
    return [0.0 for _ in range(length)]


def vector_ones(length):
    return [1.0 for _ in range(length)]


def vector_diagonal(a):
    return [a[i][i] for i in range(len(a))]


def copy_vector(vector):
    return [elem for elem in vector]


def vector_minus_vector(a, b):
    return [(a[i] - b[i]) for i in range(len(a))]


def vector_plus_vector(a, b):
    return [(a[i] + b[i]) for i in range(len(a))]


def norm(vector):
    return sum(map(lambda x: x ** 2, vector)) ** 0.5
