def copy_matrix(matrix):
    return [[elem for elem in row] for row in matrix]


def matrix_minus_matrix(a, b):
    return [[a[i][j] - b[i][j] for j in range(a[0])] for i in range(a)]


def matrix_plus_matrix(a, b):
    return [[a[i][j] + b[i][j] for j in range(a[0])] for i in range(a)]


def matrix_zeros(x, y):
    return [[0 for _ in range(x)] for _ in range(y)]


def diagonal_to_square_matrix(vector):
    tmp = matrix_zeros(len(vector), len(vector))
    for i in range(len(vector)):
        tmp[i][i] = vector[i]
    return tmp
