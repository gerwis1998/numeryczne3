from Utils.MatrixUtils import *
from Utils.VectorUtils import *


def lu_factorization(a, b):
    # init
    m = len(a)

    matrix_a = copy_matrix(a)
    matrix_l = diagonal_to_square_matrix(vector_ones(m))
    matrix_u = matrix_zeros(m, m)

    vector_b = copy_vector(b)
    vector_x = vector_ones(m)
    vector_y = vector_zeros(m)

    # LUx = b
    for j in range(m):
        for i in range(j + 1):
            matrix_u[i][j] += matrix_a[i][j]
            for k in range(i):
                matrix_u[i][j] -= matrix_l[i][k] * matrix_u[k][j]

        for i in range(j + 1, m):
            for k in range(j):
                matrix_l[i][j] -= matrix_l[i][k] * matrix_u[k][j]
            matrix_l[i][j] += matrix_a[i][j]
            matrix_l[i][j] /= matrix_u[j][j]

    # 3. perform - solve Ly = b
    for i in range(m):
        value = vector_b[i]
        for j in range(i):
            value -= matrix_l[i][j] * vector_y[j]
        vector_y[i] = value / matrix_l[i][i]

    # 4. perform - solve Ux = y
    for i in range(m - 1, -1, -1):
        value = vector_y[i]
        for j in range(i + 1, m):
            value -= matrix_u[i][j] * vector_x[j]
        vector_x[i] = value / matrix_u[i][i]

    # results
    __res = vector_minus_vector(dot_product(matrix_a, vector_x), vector_b)
    return vector_x


def gauss_seidel(a, b):
    # init
    counter = 0
    matrix_a = copy_matrix(a)
    vector_b = copy_vector(b)
    vector_x = vector_zeros(len(matrix_a[0]))

    # preform
    while True:
        for i in range(len(matrix_a)):
            tmp = vector_b[i]
            for j in range(len(matrix_a)):
                if i != j:
                    tmp -= matrix_a[i][j] * vector_x[j]
            tmp /= matrix_a[i][i]
            vector_x[i] = tmp

        res = vector_minus_vector(dot_product(matrix_a, vector_x), vector_b)
        if norm(res) < pow(10, -9):
            break
        counter += 1

    # results

    return res


def dot_product(a, b):
    copy_a = copy_matrix(a)
    copy_b = copy_vector(b)
    m = len(copy_a)
    n = len(copy_a[0])
    c = vector_zeros(m)
    for i in range(m):
        for j in range(n):
            c[i] += copy_a[i][j] * copy_b[j]
    return c
